package com.gmail.lJuanGBMinecraft.Commands;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.Utils.ExecuteUtils;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class IfCommand extends AbstractCommand{
	
	public static final String thenId = "then";
	public static final String elseId = "else";
	
	
	@Override
	public boolean command(String[] args, CommandSender sender) {

		ImmutableLocation loc = MiscUtils.getImmutableLocation(sender);

		int indexOfThen = ArrayUtils.indexOf(args, thenId);
		int indexOfElse = ArrayUtils.indexOf(args, elseId);
		
		Validate.isTrue(indexOfThen > -1, "The if command needs to have a \" " + thenId +"\"");
		
		Validate.isTrue(indexOfThen != 0, "You must define a condition");
		
		Validate.isTrue(indexOfThen != args.length && (indexOfElse < 0 || indexOfElse != indexOfThen + 1 ),
				"You must write a command to execute");

		Validate.isTrue(indexOfElse != args.length, "You must write a command to execute if you write the key word else.");
		

		String[] ifArgs = MiscUtils.subarray(args, 0, indexOfThen);
		String numberedJoinedIf   = ParseUtils.replaceVars(MiscUtils.join(ifArgs), loc);
		
		boolean activate = (0 != MiscUtils.operate(numberedJoinedIf));	

		if(activate) {
				
			String[] thenArgs;
			
			if(indexOfElse == -1) {
				thenArgs = MiscUtils.subarray(args, indexOfThen + 1, args.length);
				
			} else {
				thenArgs = MiscUtils.subarray(args, indexOfThen + 1, indexOfElse);
			}
			
			String joinedThen = MiscUtils.join(thenArgs);
			if(joinedThen.startsWith("{")) joinedThen = "/executeblock " + joinedThen;
			
			ExecuteUtils.executeCommands( sender, joinedThen);
			
		} else if(indexOfElse != -1){

			String[] elseArgs = MiscUtils.subarray(args, indexOfElse + 1, args.length);
			
			String joinedElse = MiscUtils.join(elseArgs);
			if(joinedElse.startsWith("{")) joinedElse = "/executeblock " + joinedElse;
			
			ExecuteUtils.executeCommands( sender, joinedElse);
		}
		
		return true;
	}

}