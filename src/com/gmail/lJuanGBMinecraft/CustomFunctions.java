package com.gmail.lJuanGBMinecraft;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.objecthunter.exp4j.function.Function;

/**
 * Static methods that hold the basic math operations and boolean operations.
 * 
 * @author lJuanGB
 */
public class CustomFunctions {

	private static Random randomGenerator;
	
	public static List<Function> customFunctions;
	
	static {
		
		randomGenerator = new Random(System.currentTimeMillis());
		customFunctions = new ArrayList<Function>();
		
		
		customFunctions.add( new Function("toDegrees", 1) {
			@Override
			public double apply(double... args) {
				return Math.toDegrees(args[0]);
			}
		});
		
		
		customFunctions.add( new Function("toRadians", 1) {
			@Override
			public double apply(double... args) {
				return Math.toRadians(args[0]);
			}
		});
		
		
		customFunctions.add( new Function("round", 1) {
			@Override
			public double apply(double... args) {
				return Math.round(args[0]);
			}
		});
		
		
		customFunctions.add( new Function("random", 1) {
			@Override
			public double apply(double... args) {
				return randomGenerator.nextInt((int) args[0]);
			}
		});
		
		
		
		customFunctions.add( new Function("max", 2) {
			@Override
			public double apply(double... args) {
				return Math.max(args[0], args[1]);
			}
		});
		
		
		customFunctions.add( new Function("min", 2){
			@Override
			public double apply(double... args) {
				return Math.min(args[0], args[1]);
			}
		});
	}
	
	
}
