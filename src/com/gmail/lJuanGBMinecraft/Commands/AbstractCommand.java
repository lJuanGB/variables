package com.gmail.lJuanGBMinecraft.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;

public abstract class AbstractCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
				
		if(!MiscUtils.hasPermission(sender)) return true;
		
		if(!MiscUtils.hasLocation(sender)) return true;
						
		try {
			
			return command(args, sender);
			
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
			sender.sendMessage(ChatColor.RED + ex.getMessage());
		}
		
		return true;
	}

	
	public abstract boolean command(String[] args, CommandSender sender) throws IllegalArgumentException;
}
