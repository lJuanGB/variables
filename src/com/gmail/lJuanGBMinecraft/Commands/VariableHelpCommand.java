package com.gmail.lJuanGBMinecraft.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class VariableHelpCommand extends AbstractCommand{
	
	public static final String link = "https://docdro.id/8wAj6kh";
	
	public boolean command(String[] args, CommandSender sender) {

		sender.sendMessage(ChatColor.DARK_AQUA + "Link to the guide: " + link);
		
		return true;
	}

}
