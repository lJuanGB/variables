package com.gmail.lJuanGBMinecraft.AsyncCommands;

import com.gmail.lJuanGBMinecraft.Utils.ExecuteUtils;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class AsyncVariableCommand extends AbstractAsyncCommand{

	@Override
	public boolean command(String[] args, ImmutableLocation loc) {
		
		if(args.length == 0) return false;

		String commandLine = ParseUtils.replaceVars(args, loc);
		ExecuteUtils.executeAsyncCommands(loc, commandLine);
		
		return true;
	}
}
