package com.gmail.lJuanGBMinecraft.Utils;

import static com.gmail.lJuanGBMinecraft.Constant.constBegin;
import static com.gmail.lJuanGBMinecraft.Constant.constEnd;
import static com.gmail.lJuanGBMinecraft.Variable.*;
import static com.gmail.lJuanGBMinecraft.Variable.varEnd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;

import com.gmail.lJuanGBMinecraft.Constant;
import com.gmail.lJuanGBMinecraft.Variable.ToStringType;
import com.gmail.lJuanGBMinecraft.VariableMap;
import com.google.common.collect.Lists;

import net.md_5.bungee.api.ChatColor;

public class ParseUtils {

	
	public static final String relativeX = "relative.x/";
	public static final String relativeY = "relative.y/";
	public static final String relativeZ = "relative.z/";
	public static final String relativeYaw = "relative.yaw/";
	public static final String relativePitch = "relative.pitch/";
	/**
	 * Recursive method to replace all variables by its values. 
	 * 
	 * Thread safe method
	 * 
	 * @param loc Location where the sender is placed
	 * @param s The string we want to replace
	 * @return
	 */
	public static String replaceVars(String s, ImmutableLocation loc, ToStringType type) throws IllegalArgumentException{
		
		s = s.replace(relativeX, "" + loc.getX());
		s = s.replace(relativeY, "" + loc.getY());
		s = s.replace(relativeZ, "" + loc.getZ());
		s = s.replace(relativeYaw, "" + loc.getYaw());
		s = s.replace(relativePitch, "" + loc.getPitch());
		
		String world = loc.getWorld();
		
		while(s.contains(varBegin)) {
			
			s = replace(s, varBegin, varEnd, world, true, (str) -> VariableMap.getVariable(world, str).toString(type));			
		}
		
		while(s.contains(constBegin)) {
			
			s = replace(s, constBegin, constEnd, world, false, (str) -> Constant.getByName(str).toString(type));
		}
		
		return s;
	}
	

	public static String replaceVars(String[] args, ImmutableLocation loc, ToStringType type) throws IllegalArgumentException {
		
		String concatenated = MiscUtils.join(args);
		
		return replaceVars(concatenated, loc, type);
	}
	
	public static String replaceVars(String[] args, ImmutableLocation loc) throws IllegalArgumentException {
		return replaceVars(args, loc, ToStringType.NORMAL);
	}
	
	public static String replaceVars(String s, ImmutableLocation loc) throws IllegalArgumentException {
		return replaceVars(s, loc, ToStringType.NORMAL);
	}

	
	public static final char openBr = '{';
	public static final char closeBr = '}';
	public static final char lineBreak = ';';
	public static final char escape = '\\';
	public static final char string = '\"';
	
	public static String[] separateCommands(String joined) throws IllegalArgumentException{
		
		List<String> commands = new ArrayList<String>();
		
		int nOpen =  StringUtils.countMatches(joined, openBr +"") - StringUtils.countMatches(joined, escape + "" + openBr);
		int nClose =  StringUtils.countMatches(joined, closeBr +"") - StringUtils.countMatches(joined, closeBr + "" + openBr);
		
		if(nOpen != nClose) {
			
			throw new IllegalArgumentException(
					ChatColor.RED + "Unbalanced number of brackets " + openBr + closeBr);
		}
		
		
		if(!joined.startsWith(openBr + "") || !joined.endsWith(closeBr + "")) {
			
			throw new IllegalArgumentException(
					ChatColor.RED + "The block must start and end with brackets "+ openBr + closeBr);
		}
		
		
		joined = joined.substring(1, joined.length() - 1);		
		
		
		int openBracket = 0;
		boolean openString = false;
		String currentCommand = "";

		for(int i = 0 ; i < joined.length(); i++) {
			
			char c = joined.charAt(i);
			
			if(i != 0 && joined.charAt(i-1) == escape) {
				
				currentCommand += c;
				continue;
			}
			
			if(openString) {
				
				currentCommand += c;
				
				if(c == string) openString = false;
				
				continue;
			}
			
			switch(c) {
			
			case lineBreak:{
				
				if(openBracket == 0) {
					commands.add(currentCommand);
					currentCommand = "";
					
				} else {
					
				}

				break;}
			
			case openBr:{
				currentCommand += c;
				openBracket++;
				break;}
			
			case closeBr:{
				if(openBracket > 0) {
					currentCommand += c;
					openBracket--;
				
				}else {
					
					throw new IllegalArgumentException(
							ChatColor.RED + "Unbalanced bracket: Closed a bracket that was not open.");
				}
				
				break;}
			
			case '/':
			case ' ':{
				
				if(!currentCommand.equals("")) currentCommand += c;
				break;}
			
			case escape: break;
			
			case string: openString = true;
			
			default: currentCommand += c;
			}
			
	    }
		
		if(!currentCommand.isEmpty()) commands.add(currentCommand);
		
		return commands.toArray(new String[0]);
	}

	public static String[] separateStrings(String joined) throws IllegalArgumentException{
		
	    List<String> tokens = new ArrayList<String>();
	    
	    boolean escaping = false;
	    char quoteChar = ' ';
	    boolean quoting = false;
	    
	    StringBuilder current = new StringBuilder() ;
	    
	    for (int i = 0; i<joined.length(); i++) {
	    	
	        char c = joined.charAt(i);
	        if (escaping) {
	            current.append(c);
	            escaping = false;
	        } else if (c == '\\' && !(quoting && quoteChar == '\'')) {
	            escaping = true;
	        } else if (quoting && c == quoteChar) {
	            quoting = false;
	        } else if (!quoting && (c == '\'' || c == '"')) {
	            quoting = true;
	            quoteChar = c;
	        } else if (!quoting && Character.isWhitespace(c)) {
	            if (current.length() > 0) {
	                tokens.add(current.toString());
	                current = new StringBuilder();
	            }
	        } else {
	            current.append(c);
	        }
	    }
	    
	    if (current.length() > 0) {
	        tokens.add(current.toString());
	    }
	    
	    return tokens.toArray(new String[0]);
	}
	
	
	public static double[] parseLoc(String[] string, ImmutableLocation loc)  throws IllegalArgumentException{
		
		if(string.length != 3) throw new IllegalArgumentException("Array length MUST be 3");
		
		double[] result = new double[3];
		
		double[] relative = new double[] {loc.getX(), loc.getY(), loc.getZ()};
		double[] carat = new double[] {-Math.sin(loc.getYaw()), -Math.sin(loc.getPitch()), Math.cos(loc.getYaw())};
		
		for(int i = 0; i < 3; i++) {
			
			boolean rel = false;
			boolean car = false;
			
			if(string[i].contains("~")) rel = true;
			if(string[i].contains("^")) car = true;
			
			string[i] = string[i].replace("~", "").replace("^", "");
			
			if(string[i].isEmpty()) string[i] = "0";
			
			double d = 0;
			try {
				
				d = Double.parseDouble(string[i]);
			} catch (NumberFormatException ex) {
				
				throw new IllegalArgumentException(ex);
			}
			
			
			if(rel) {
				d += relative[i];
				
			} else if(car) {
				
				d = d*carat[i] + relative[i];
			}
			
			result[i] = d;
			
			if(d > 30000000) throw new IllegalArgumentException("Location above minecraft generation limits.");
		}
		
		return result;
	}
	
	public static String[] parseParameters(String[] args, Parameter<?>[] params) {
		
		Validate.isTrue(Parameter.getExpectedWords(params) == args.length, 
				"Expected " + Parameter.getExpectedWords(params) + " arguments in order to parse: " + Arrays.toString(params));
		
		int argsIndex = 0;
		List<String> result = Lists.newArrayListWithExpectedSize(params.length);
		
		for(Parameter<?> par : params) {
			
			int words = par.getWords();
			
			result.add( MiscUtils.join( MiscUtils.subarray(args, argsIndex, argsIndex + words)) );
			
			argsIndex += words;
		}
		
		return result.toArray(new String[0]);
	}
	
	public static String[] forbiddenWords = new String[] 
			{
					relativeX, relativeY, relativeZ, relativeYaw, relativePitch,
					openBr +"", closeBr +"", lineBreak +"",
					constBegin, constEnd,
					varBegin, varEnd,
			};
	
	public static boolean hasForbiddenWord(String s) {
		
		for(String forbidden : forbiddenWords) {
			if(s.contains(forbidden)) return true;
		}
		
		return false;
	}
	
	public static String replaceIndex(String s, String world) {
		
		while(s.contains(indexBegin)) {

			s = replace(s, indexBegin, indexEnd, world, false, (str) -> VariableMap.getVariable(world, str).toString());
		}
		
		return s;
	}
	
	private static String replace(String s, String begin, String end, String world, boolean replaceIndex, Function<String, String> function ) {
		
		int indexOfEnding = s.indexOf(end, s.indexOf(begin)) + end.length();
		
		if(indexOfEnding <= 0) {
			throw new IllegalArgumentException("Declarator found \"" + begin +
					"\" but no closing \"" + end + "\" found.");
		}
		
		String variable = s.substring(s.indexOf(begin), indexOfEnding);
		
		if(replaceIndex) variable = replaceIndex(variable, world);

		return s.replace(variable, function.apply(variable) );
		
	}
	

	
}
