package com.gmail.lJuanGBMinecraft;

import static com.gmail.lJuanGBMinecraft.Variable.indexBegin;
import static com.gmail.lJuanGBMinecraft.Variable.indexEnd;
import static com.gmail.lJuanGBMinecraft.Variable.varBegin;
import static com.gmail.lJuanGBMinecraft.Variable.varEnd;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang.Validate;
import org.bukkit.World;

import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

/**
 * Storage for all the variables
 * 
 * @author lJuanGB
 */
public class VariableMap {

	/**
	 * A key system that allows easy search for variables per world, project and name
	 * 
	 * @author lJuanGB
	 */
	public static class VariableKey{
		
		private final String worldName;
		private final String projectName;
		private final String variableName;

		public static final String separator = ".";
		
		public VariableKey(String world, String projectName, String variableName) {
			this.worldName = world;
			this.projectName = projectName;
			this.variableName = variableName;
		}
		
		public VariableKey(World world, String projectName, String variableName) {
			this(world.getName(), projectName, variableName);
		}
		
		public VariableKey(String world, String variable) {
			this(world, 
				separate( variable )[0], 
				separate( variable )[1]);
		}
		
		public VariableKey(World world, String variable) {
			this(world.getName(), variable);
		}
		
		public String getWorld() {
			return this.worldName;
		}
		
		public String getProject() {
			return this.projectName;
		}

		
		@Override
		public String toString() {
			return this.getProject()+separator+this.variableName;
		}
		
		public String toFullString() {
			return this.getWorld() + separator + toString();
		}
		
		@Override
		public int hashCode() {
			String string = this.getWorld()+separator+this.getProject()+separator+this.variableName;
			
			return string.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			if(this == obj) return true;
			
			if(!(obj instanceof VariableKey)) return false;
			
			VariableKey key2 = (VariableKey) obj;
			if(!this.worldName.equals(key2.worldName)) return false;
			if(!this.projectName.equals(key2.projectName)) return false;
			if(!this.variableName.equals(key2.variableName)) return false;
			
			return true;
		}
		
		public static boolean isKey(String s) {
			return s.contains(separator);
		}
		
		private static String[] separate(String variable) throws IllegalArgumentException{
			
			variable = variable.replace(varBegin, "").replace(varEnd, "").replace(indexBegin, "").replace(indexEnd, "");
			
			if(!isKey(variable)) {
				throw new IllegalArgumentException("Variable name: \""+ variable + "\" does NOT contain a \"" + separator +"\"");
			}
			
			String[] args = new String[2];
			
			args[0] = variable.substring(0, variable.indexOf(separator));
			args[1] = variable.substring(variable.indexOf(separator) + 1, variable.length());
			
			return args;
		}
	}
	
	
	public static Thread main;
	public static ConcurrentMap<Thread, ConcurrentMap<VariableKey, Variable>> map;
	public static int threadCount;
	
	static {
		reset();
	}
	

	
	public static Variable getVariable(VariableKey key) throws IllegalArgumentException{
		
		Validate.isTrue(!ParseUtils.hasForbiddenWord(key.toString()),
				"Error getting variable " + key + ": The variable key contains a forbidden word or character.");
		
		return getMap().computeIfAbsent(key, v -> new Variable(0));
		
	}
	
	/**
	 * Method that allows reading the variable from the format that is imputed in commands
	 * 
	 * @param world The world where the project is stored
	 * @param variable The variable in the format "project.name"
	 * @return The variable.
	 */
	public static Variable getVariable(String world, String variable) throws IllegalArgumentException{

		VariableKey key = new VariableKey(world, variable);

		return getVariable(key);
	}
	
	public static Variable getVariable(World world, String variable) throws IllegalArgumentException{
		return getVariable(world.getName(), variable);
	}
	
	
	public static List<VariableKey> getKeysUnderKey(VariableKey key) {
		
		List<VariableKey> keys = new ArrayList<VariableKey>();
		
		for(VariableKey mapKey : getMap().keySet()) {
			
			if(!mapKey.equals(key) && mapKey.toFullString().startsWith(key.toFullString())) {
				keys.add(key);
			}
			
		}
		
		sort(keys);
		return keys;
	}
	
	public static List<VariableKey> getKeysInProject(String world, String project) {
		
		List<VariableKey> keys = new ArrayList<VariableKey>();
		
		for(VariableKey key : getMap().keySet()) {
			
			if(key.getWorld().equals(world) && key.getProject().equals(project)) {
				keys.add(key);
			}
			
		}
		
		sort(keys);
		return keys;
	}
	
	public static List<VariableKey> getKeysInProject(World world, String project) {
		return getKeysInProject(world.getName(), project);
	}

	
	public static List<String> getProjectsInWorld(String world) {
		
		List<String> projects = new ArrayList<String>();
		
		for(VariableKey key : getMap().keySet()) {
			
			if(key.getWorld().equals(world) && !projects.contains(key.getProject())) {
				projects.add(key.getProject());
			}
			
		}
		
		projects.sort((s1, s2) -> s1.compareToIgnoreCase(s2));
		return projects;
	}
	
	public static List<String> getProjectsInWorld(World world) {
		return getProjectsInWorld(world.getName());
	}
	
	
	
	public static void removeKey(VariableKey key) {
		
		getMap().remove(key);
	}
	
	public static void removeKeys(VariableKey... keys) {
		
		for(VariableKey k : keys) removeKey(k);
	}
	
	public static void removeKeys(Iterable<VariableKey> keys) {
		
		for(VariableKey k : keys) removeKey(k);
	}

	
	public static void removeUnderKey(VariableKey key) {
		
		removeKeys(getKeysUnderKey(key));
	}

	public static void removeInProject(String world, String project) {
		
		removeKeys(getKeysInProject(world, project));
	}
	
	public static void removeInProject(World world, String project) {
		removeInProject(world.getName(), project);
	}
	
	public static void removeInWorld(String world) {
		
		for(String project : getProjectsInWorld(world)) removeKeys(getKeysInProject(world, project));
	}
	
	public static void removeInWorld(World world) {
		removeInWorld(world.getName());
	}

	
	
	
	private static ConcurrentMap<VariableKey, Variable> getMainMap() {
		
		return map.get(main);
	}
	
	private static ConcurrentMap<VariableKey, Variable> getMap() {
		
		Thread thread = Thread.currentThread();
		return map.computeIfAbsent(thread, x -> threadBorn());
	}
	
	
	public static void addThreadCount() {
		threadCount++;
	}
	
	public static void removeThreadCount() {
		threadCount--;
		
		threadCount = Math.max(1, threadCount);
	}

	public static ConcurrentMap<VariableKey, Variable> threadBorn() {
		
		Thread thread = Thread.currentThread();
		ConcurrentMap<VariableKey, Variable> copy = new ConcurrentHashMap<VariableKey, Variable>();

		for(VariableKey key : getMainMap().keySet()) {
			copy.put(key, new Variable(getMainMap().get(key)));
		}
		
		VariableMap.map.put(thread, copy);
				
		return copy;
	}
	
	public static void threadDied() {
		
		Thread thread = Thread.currentThread();
		VariableMap.map.remove(thread);
		
	}
	
	
	public static void start() {
		
		main = Thread.currentThread();
				
		VariableMap.map.put(main, new ConcurrentHashMap<VariableKey, Variable>());
		
		threadCount++;
		
	}
	
	public static void reset() {
		
		VariableMap.map = new ConcurrentHashMap<Thread, ConcurrentMap<VariableKey, Variable>>();
	}
	
	public static void remove() {
		
		VariableMap.map = null;
		main = null;
	}
	
	
	private static void sort(List<VariableKey> list){
		
		list.sort((k1, k2) -> k1.toString().compareToIgnoreCase(k2.toString()));
	}
}
