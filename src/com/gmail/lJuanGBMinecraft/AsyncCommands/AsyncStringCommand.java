package com.gmail.lJuanGBMinecraft.AsyncCommands;

import org.apache.commons.lang.Validate;

import com.gmail.lJuanGBMinecraft.StringMethod;
import com.gmail.lJuanGBMinecraft.Variable;
import com.gmail.lJuanGBMinecraft.Variable.ToStringType;
import com.gmail.lJuanGBMinecraft.VariableMap;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class AsyncStringCommand extends AbstractAsyncCommand{
	
	@Override
	public boolean command( String[] args, ImmutableLocation loc) {
		
		Validate.isTrue(args.length > 2, "At least 3 arguments must be defined");

		Variable var = VariableMap.getVariable(loc.getWorld(), args[0]);

		String[] trimedArray = MiscUtils.subarray(args, 2, args.length);
		String numberedJoined = ParseUtils.replaceVars(trimedArray, loc, ToStringType.QUOTED);
		String[] separated = ParseUtils.separateStrings(numberedJoined);
		
		String result = "";
		
		if(separated.length == 1) {
			
			result = separated[0];
			
		} else {
			
			StringMethod meth = StringMethod.fromString(separated[0]);
			
			String[] methArgs = MiscUtils.subarray(separated, 1, separated.length);
			
			Validate.isTrue(meth.isValid(methArgs), meth.getInValidReason(methArgs));

			result = meth.apply(methArgs);
		}
		
		var.set(result);
		return true;
	}
	
}