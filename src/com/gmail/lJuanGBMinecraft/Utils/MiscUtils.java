package com.gmail.lJuanGBMinecraft.Utils;

import static org.bukkit.Particle.BLOCK_DUST;
import static org.bukkit.Particle.CRIT_MAGIC;
import static org.bukkit.Particle.DRIP_LAVA;
import static org.bukkit.Particle.DRIP_WATER;
import static org.bukkit.Particle.ENCHANTMENT_TABLE;
import static org.bukkit.Particle.EXPLOSION_HUGE;
import static org.bukkit.Particle.EXPLOSION_LARGE;
import static org.bukkit.Particle.FIREWORKS_SPARK;
import static org.bukkit.Particle.ITEM_CRACK;
import static org.bukkit.Particle.MOB_APPEARANCE;
import static org.bukkit.Particle.REDSTONE;
import static org.bukkit.Particle.SLIME;
import static org.bukkit.Particle.SMOKE_LARGE;
import static org.bukkit.Particle.SMOKE_NORMAL;
import static org.bukkit.Particle.SNOWBALL;
import static org.bukkit.Particle.SPELL;
import static org.bukkit.Particle.SPELL_INSTANT;
import static org.bukkit.Particle.SPELL_MOB;
import static org.bukkit.Particle.SPELL_MOB_AMBIENT;
import static org.bukkit.Particle.SUSPENDED;
import static org.bukkit.Particle.TOTEM;
import static org.bukkit.Particle.TOWN_AURA;
import static org.bukkit.Particle.VILLAGER_ANGRY;
import static org.bukkit.Particle.VILLAGER_HAPPY;
import static org.bukkit.Particle.WATER_BUBBLE;
import static org.bukkit.Particle.WATER_DROP;
import static org.bukkit.Particle.WATER_SPLASH;
import static org.bukkit.Particle.WATER_WAKE;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ProxiedCommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.minecart.CommandMinecart;

import com.gmail.lJuanGBMinecraft.CustomFunctions;
import com.gmail.lJuanGBMinecraft.CustomOperators;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import net.objecthunter.exp4j.ValidationResult;

public class MiscUtils {

	
	public static double operate(String... args) throws IllegalArgumentException {
		
		String joined = "";
		for(String s : args) {
			joined += s;
		}
		
		Expression expr = new ExpressionBuilder(joined)
				.operator(CustomOperators.customOperators)
				.functions(CustomFunctions.customFunctions)
				.build();
		
		ValidationResult valid = expr.validate();
		
		if(valid.isValid()) {
			
			try {
				
				return expr.evaluate();
				
			} catch(ArithmeticException ex) {
				
				throw new IllegalArgumentException(ex);
			}
			
		} else {
			throw new IllegalArgumentException(
					valid.getErrors().toString());
		}
	}
	
	public static boolean hasPermission(CommandSender sender) {
		return sender.isOp();
	}
	
	public static boolean hasLocation(CommandSender sender) {
		
		if(sender instanceof Player) return true;
		if(sender instanceof BlockCommandSender) return true;
		if(sender instanceof ProxiedCommandSender) {
			
			if(((ProxiedCommandSender) sender).getCallee() instanceof Entity) return true;
		}
		if(sender instanceof CommandMinecart) return true;
		
		return false;
	}
	
	public static Location getLocation(CommandSender sender) {
		
		if(!hasLocation(sender)) return null;
		
		if(sender instanceof Player) {
				
			return ((Player) sender).getLocation();
		} 
		
		if (sender instanceof ProxiedCommandSender) {
			
			ProxiedCommandSender proxSender = (ProxiedCommandSender) sender;
		
			if(proxSender.getCallee() instanceof Entity) {
				
				return ((Entity) proxSender.getCallee()).getLocation();
				
			} else {
				
				return null;
			}
		}
		
		if (sender instanceof BlockCommandSender) {
			
			return ((BlockCommandSender) sender).getBlock().getLocation();
		}
		
		if(sender instanceof CommandMinecart) {
			
			return ((CommandMinecart) sender).getLocation();
		}


		return null;
	}
	
	public static ImmutableLocation getImmutableLocation(CommandSender sender) {
		return new ImmutableLocation(getLocation(sender));
	}
	
	public static String join(String... args) {
		
		String result = "";
		
		for(int i = 0; i < args.length; i++) {
			
			if(i == 0) {
				
				result += args[i];
			} else {
				
				result += " " + args[i];
			}
		}
		
		return result;
	}
	
	public static Particle getParticle(String particle) throws IllegalArgumentException{
		
		particle = particle.replaceFirst("minecraft:", "");
		
		switch(particle.toLowerCase()) {
		
		case "ambient_entity_effect": return SPELL_MOB_AMBIENT;
		case "angry_villager": return VILLAGER_ANGRY;
		case "block": return BLOCK_DUST;
		case "bubble": return WATER_BUBBLE;
		case "dripping_lava": return DRIP_LAVA;
		case "dripping_water": return DRIP_WATER;
		case "dust": return REDSTONE;
		case "effect": return SPELL;
		case "elder_guardian": return MOB_APPEARANCE;
		case "enchant": return ENCHANTMENT_TABLE;
		case "enchanted_hit": return CRIT_MAGIC;
		case "entity_effect": return SPELL_MOB;
		case "explosion": return EXPLOSION_LARGE;
		case "explosion_emitter": return EXPLOSION_HUGE;
		case "firework": return FIREWORKS_SPARK;
		case "fishing": return WATER_WAKE;
		case "happy_villager": return VILLAGER_HAPPY;
		case "instant_effect": return SPELL_INSTANT;
		case "item": return ITEM_CRACK;
		case "item_slime": return SLIME;
		case "item_snowball": return SNOWBALL;
		case "large_smoke": return SMOKE_LARGE;
		case "mycelium": return TOWN_AURA;
		case "poof": return MOB_APPEARANCE;
		case "rain": return WATER_DROP;
		case "smoke": return SMOKE_NORMAL;
		case "splash": return WATER_SPLASH;
		case "totem_of_undying": return TOTEM;
		case "underwater": return SUSPENDED;
		}
		
		return Particle.valueOf(particle.toUpperCase());
	}
	
	
	public static String[] subarray(String[] args, int indexStartInclusive, int indexEndExclusive) {
		return (String[]) ArrayUtils.subarray(args, indexStartInclusive, indexEndExclusive);
	}
	
	public static String[] remove(String[] args, int index) {
		return (String[]) ArrayUtils.remove(args, index);
	}
	
}
