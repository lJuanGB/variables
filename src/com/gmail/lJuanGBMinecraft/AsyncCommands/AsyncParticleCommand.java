package com.gmail.lJuanGBMinecraft.AsyncCommands;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Particle.DustOptions;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class AsyncParticleCommand  extends AbstractAsyncCommand{

	@Override
	public boolean command(String[] args, ImmutableLocation loc) {

		Validate.isTrue(args.length > 0, "That command needs at least 1 argument");
		
		Particle part = MiscUtils.getParticle(args[0]);
			
		int argOff = getArgumentOffset(part);
		
		Validate.isTrue(args.length > (8 + argOff), "That command needs at least " + 8 + argOff + " arguments.");

		boolean force = false;		
		double[] xyz = ParseUtils.parseLoc(MiscUtils.subarray(args, 1 + argOff, 4 + argOff), loc);
		double offX = parseDouble(args[4 + argOff]);
		double offY = parseDouble(args[5 + argOff]);
		double offZ = parseDouble(args[6 + argOff]);
		double speed = parseDouble(args[7 + argOff]);
		int count = parseInteger(args[8 + argOff]);

		if(args.length > (9 + argOff) && args[9 + argOff].equals("force")) force = true;

		World worldObj = Bukkit.getWorld(loc.getWorld());
		Validate.notNull(worldObj, "The world has unloaded or no longer exists");
		
		Object data = null;
		
		switch(part) {
		
		case REDSTONE: {
			
			int red = getColor(args[1]);
			int green = getColor(args[2]);
			int blue = getColor(args[3]);
			float size = (float) parseDouble(args[4]);				
			
			data = new DustOptions(Color.fromRGB(red, green, blue), size);
						
			break;}
		
		
		case ITEM_CRACK: {
			
			Material mat =  Material.valueOf(args[1].replace("minecraft:", "").toUpperCase());
			
			data = new ItemStack(mat);
			
			break;}
		
		
		case BLOCK_DUST:
		case FALLING_DUST:
		case BLOCK_CRACK: {
				
			data = Bukkit.getServer().createBlockData(args[1]);
						
			break;}
		
		default:
			break;
		}		
		
		worldObj.spawnParticle(part, xyz[0], xyz[1], xyz[2], count, offX, offY, offZ, speed, data, force);

		return true;
	}

	private int getArgumentOffset(Particle part) {

		switch(part) {
		
		case REDSTONE: return 4;
		case FALLING_DUST:
		case ITEM_CRACK:
		case BLOCK_DUST:
		case BLOCK_CRACK: return 1;

		default: return 0;
		}
	}
	
	private int getColor(String s) throws IllegalArgumentException{
		
		double d = Double.parseDouble(s);
		return (int) Math.round( Math.min(Math.max(0, d), 255));
	}
	
	private double parseDouble(String s) throws IllegalArgumentException{
		try {
			
			return Double.parseDouble(s);
			
		} catch (NumberFormatException ex) {
			
			throw new IllegalArgumentException(ex);
		}
	}
	
	private int parseInteger(String s) throws IllegalArgumentException{
		try {
			
			return Integer.parseInt(s);
			
		} catch (NumberFormatException ex) {
			
			throw new IllegalArgumentException(ex);
		}
	}
}
