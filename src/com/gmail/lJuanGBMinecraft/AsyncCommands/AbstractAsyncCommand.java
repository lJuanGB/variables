package com.gmail.lJuanGBMinecraft.AsyncCommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.lJuanGBMinecraft.VariableMap;
import com.gmail.lJuanGBMinecraft.VariablesStartUp;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;

public abstract class AbstractAsyncCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
				
		if(!MiscUtils.hasPermission(sender)) return true;
		
		if(isBusy()) {
			sender.sendMessage(ChatColor.RED + "Async thread is busy.");
			return true;
		}
		
		if(!MiscUtils.hasLocation(sender)) return true;
				
		final ImmutableLocation loc = MiscUtils.getImmutableLocation(sender);
		
		VariableMap.addThreadCount();
		
		new BukkitRunnable()
		
		{
			@Override
			public void run() {
				
				VariableMap.threadBorn();
				try {
					command(args, loc);
					
				} catch (IllegalArgumentException ex) {
					
					ex.printStackTrace();
					Bukkit.getScheduler().callSyncMethod(
							VariablesStartUp.plugin, 
							() -> {sender.sendMessage(ChatColor.RED + ex.getMessage()); return true;});
				}

				VariableMap.threadDied();
				VariableMap.removeThreadCount();
			}
			
		}
		
		.runTaskAsynchronously(VariablesStartUp.plugin);
		return true;
	}
	
	
	public static final int threadLimit = 1;
	
	private boolean isBusy() {
		
		return VariableMap.threadCount > threadLimit;
	}
	
	public abstract boolean command(String[] args, ImmutableLocation loc) throws IllegalArgumentException;
	
}
