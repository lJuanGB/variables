package com.gmail.lJuanGBMinecraft.Commands;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.Utils.ExecuteUtils;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class WhileCommand extends AbstractCommand{
	
	public static final String doId = "do";
	
	private int limit;
	
	public WhileCommand(int limit) {
		this.limit = limit;
	}
	
	public WhileCommand() {
		this(1001);
	}
	
	@Override
	public boolean command(String[] args, CommandSender sender) {
		
		ImmutableLocation loc = MiscUtils.getImmutableLocation(sender);
	
		int indexDo   = ArrayUtils.indexOf(args, doId);
		
		Validate.isTrue(indexDo > -1, "The while command should contain " + doId);	
		
		String[] conditional = MiscUtils.subarray(args, 0, indexDo);
		
		String[] command = MiscUtils.subarray(args, indexDo + 1, args.length);
				
		String joinedCommand = MiscUtils.join( command );
		if(joinedCommand.startsWith("{")) joinedCommand = "/executeblock " + joinedCommand;

		
		boolean activate = 0 != MiscUtils.operate( ParseUtils.replaceVars(conditional, loc) );
		
		int count = 0;
		while(activate) {
			
			ExecuteUtils.executeCommands(sender, joinedCommand);
			
			count++;
			Validate.isTrue(count < limit, "REACHED SECURITY LOOP LIMIT: " + limit);
			
			activate = 0 != MiscUtils.operate( ParseUtils.replaceVars(conditional, loc) );
		}

		return true;
	}
	
}