package com.gmail.lJuanGBMinecraft.Commands;

import org.apache.commons.lang.Validate;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.Utils.ExecuteUtils;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class ExecuteBlockCommand extends AbstractCommand{
	
	@Override
	public boolean command(String[] args, CommandSender sender) {
		
		int l = args.length;
		
		Validate.isTrue(l != 0, "You need at least 1 argument");
		Validate.isTrue(args[0].startsWith("{"), "The arguments must start with {");
		Validate.isTrue(args[l-1].endsWith("}"), "The arguments must start with {");
		
		String joined = MiscUtils.join(args);
		
		String[] commands = ParseUtils.separateCommands(joined);
		
		ExecuteUtils.executeCommands(sender, commands);
		return true;
	}
}
