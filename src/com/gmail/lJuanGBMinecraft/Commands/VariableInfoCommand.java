package com.gmail.lJuanGBMinecraft.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.VariableMap;
import com.gmail.lJuanGBMinecraft.VariableMap.VariableKey;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;

public class VariableInfoCommand extends AbstractCommand{
	
	@Override
	public boolean command(String[] args, CommandSender sender) {
		
		Location loc = MiscUtils.getLocation(sender);
		
		if(args.length == 0) {
			
			sendWorld(loc.getWorld(), sender, 0);

		} else if(args[0].endsWith(".*")) {
			
			VariableKey key = new VariableKey(loc.getWorld(), args[0].replaceAll(".*$", ""));

			sendUnder(key, sender, 0);

		} else if(args[0].contains(".")) {
			
			VariableKey key = new VariableKey(loc.getWorld(), args[0]);

			sendVariable(key, sender, 0);
			
		} else if(args[0].equals("server")) {
			
			sendAll(sender, 0);
		
		} else {
			
			sendProject(loc.getWorld().getName(), args[0], sender,  0);
			
		}
		
		return true;
	}
	
	
	private void sendUnder(VariableKey key, CommandSender sender, int indentation) {
		
		String message = ChatColor.BOLD + "" + ChatColor.GOLD;
		
		for(int i = 0; i < indentation; i++) message += " "; 
		
		message += "Variables under "+ ChatColor.ITALIC + key.toString()+ " :";
		sender.sendMessage(message);
		
		for(VariableKey mapKey : VariableMap.getKeysUnderKey( key) ) {
			sendVariable(mapKey, sender, indentation + 2);
		}
		
	}

	private void sendVariable(VariableKey key, CommandSender sender, int indentation) {
		
		String message = ChatColor.DARK_AQUA + "";
		
		for(int i = 0; i < indentation; i++) message += " "; 
		
		message += key.toString() + " = " + VariableMap.getVariable(key);
		sender.sendMessage(message);
	}
	
	private void sendProject(String world, String project, CommandSender sender, int indentation) {
		
		String message = ChatColor.BOLD + "" + ChatColor.GOLD;
		
		for(int i = 0; i < indentation; i++) message += " "; 
		
		message += "Variables in the project "+ ChatColor.ITALIC + project + " :";
		sender.sendMessage(message);
		
		for(VariableKey key : VariableMap.getKeysInProject( world , project) ) {
			sendVariable(key, sender, indentation + 2);
		}
	}
	
	private void sendWorld(World world, CommandSender sender, int indentation) {
		
		String message = ChatColor.BOLD + "" + ChatColor.DARK_PURPLE;
		
		for(int i = 0; i < indentation; i++) message += " "; 
		
		message += "Variables in the world "+ ChatColor.ITALIC + world.getName() + " :";
		sender.sendMessage(message);
		
		for(String project : VariableMap.getProjectsInWorld(world) ) {
			sendProject(world.getName(), project, sender, indentation + 2);
			sender.sendMessage("");
		}
	}
	
	private void sendAll(CommandSender sender, int indentation) {
		
		String message = ChatColor.BOLD + "" + ChatColor.RED;
		
		for(int i = 0; i < indentation; i++) message += " "; 
		
		message += "Variables the server :";
		sender.sendMessage(message);
		
		for(World w : Bukkit.getWorlds() ) {
			sendWorld(w, sender, indentation + 2);
		}
		
	}
}
