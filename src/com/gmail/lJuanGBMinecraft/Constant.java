package com.gmail.lJuanGBMinecraft;

import com.gmail.lJuanGBMinecraft.Variable.ToStringType;

public enum Constant {

	PI(Math.PI),
	E(Math.E),
	PHI(1.6180339887498948482d),
	INTEGER_MAX(Integer.MAX_VALUE),
	INTEGER_MIN(Integer.MIN_VALUE),
	SHORT_MAX(Short.MAX_VALUE),
	SHORT_MIN(Short.MIN_VALUE),
	BYTE_MAX(Byte.MAX_VALUE),
	BYTE_MIN(Byte.MIN_VALUE),
	ZERO(0.0),
	;
	
	private final Variable var;
	
	private Constant(double value) {
		this.var = new Variable(value);
	}
	
	private Constant(String value) {
		this.var = new Variable(value);
	}
	
	
	public static final String constBegin = "const.";
	public static final String constEnd = "/";
	
	public static Constant getByName(String constant) {
		
		constant = constant.replace(constEnd, "").replace(constBegin, "").toUpperCase() ;
		
		try {
			
			return Constant.valueOf(constant);
			
		} catch (IllegalArgumentException ex) {
			
			return Constant.ZERO;
			
		}
	}
	
	
	@Override
	public String toString() {
		
		return var.toString(ToStringType.NORMAL);
	}
	
	public String toString(ToStringType type) {
		
		return var.toString(type);
	}
}
