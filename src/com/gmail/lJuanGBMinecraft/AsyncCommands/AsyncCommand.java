package com.gmail.lJuanGBMinecraft.AsyncCommands;

import java.util.Map;

import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.google.common.collect.Maps;

public enum AsyncCommand {

	AMATH(AsyncMathCommand.class, "math", "asyncmath"),
	AIF(AsyncIfCommand.class, "if", "asyncif"),
	AEXECUTEBLOCK(AsyncExecuteBlockCommand.class, "executeblock", "eblock", "executeb", "aeblock", "aexecuteb"),
	APARTICLE(AsyncParticleCommand.class, "particle"),
	AFOR(AsyncForCommand.class, "for"),
	AWHILE(AsyncWhileCommand.class, "while"),
	AVARIABLE(AsyncVariableCommand.class, "variable", "avar", "var"),
	ALIMITLESSFOR(new AsyncForCommand(Integer.MAX_VALUE), "limitlessfor"),
	ALIMITLESSWHILE(new AsyncWhileCommand(Integer.MAX_VALUE), "limitlesswhile"),
	ASTRING(AsyncStringCommand.class),
	;
	
	
	private final AbstractAsyncCommand cmd;
	private final String[] alias;
	
	private AsyncCommand(AbstractAsyncCommand cmd, String... alias) {
		
		this.cmd = cmd;
		this.alias = alias;
	}
	
	private AsyncCommand(Class<? extends AbstractAsyncCommand> clazz, String... alias) {
		
		AbstractAsyncCommand cmd = null;
		try {
			cmd = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		this.cmd = cmd;
		
		this.alias = alias;
	}
	

	
	
	public void runCommand(String[] args, ImmutableLocation loc) throws IllegalArgumentException{
		
		cmd.command(args, loc);
	}
	
	
	private static final Map<String, AsyncCommand> byName = Maps.newHashMap();
	
	static {
		
		for(AsyncCommand cmd : values()) {
			
			byName.put(cmd.name().toLowerCase(), cmd);
			
			for(String s : cmd.alias) {
				byName.put(s, cmd);
			}
		}
	}
	
	public static AsyncCommand byName(String name) {
		return byName.get(name.toLowerCase());
	}
	
	/*
  amath:
    description: Do '/variablehelp' for help on how to use this
    usage: "\u00A7cSyntax error! Do '/variablehelp' to get help on how to use this command"
    aliases: [asyncmath]
  aif:
    description: Do '/variablehelp' for help on how to use this
    usage: "\u00A7cSyntax error! Do '/variablehelp' to get help on how to use this command"
    aliases: [asyncif]
  aexecuteblock:
    description: Do '/variablehelp' for help on how to use this
    usage: "\u00A7cSyntax error! Do '/variablehelp' to get help on how to use this command"
    aliases: [aeblock, aexecuteb, asyncebloc, asyncexecuteb]
  aparticle:
    description: Do '/variablehelp' for help on how to use this
    usage: "\u00A7cSyntax error! Do '/variablehelp' to get help on how to use this command"
    aliases: [asyncparticle]
  afor:
    description: Do '/variablehelp' for help on how to use this
    usage: "\u00A7cSyntax error! Do '/variablehelp' to get help on how to use this command"
    aliases: [asyncfor]
  awhile:
    description: Do '/variablehelp' for help on how to use this
    usage: "\u00A7cSyntax error! Do '/variablehelp' to get help on how to use this command"
    aliases: [asyncwhile]
  avariable:
    description: Do '/variablehelp' for help on how to use this
    usage: "\u00A7cSyntax error! Do '/variablehelp' to get help on how to use this command"
    aliases: [asyncvariable, avar, asyncvar]
  limitlessafor:
    description: Do '/variablehelp' for help on how to use this
    usage: "\u00A7cSyntax error! Do '/variablehelp' to get help on how to use this command"
    aliases: [limitlessasyncfor]
  limitlessawhile:
    description: Do '/variablehelp' for help on how to use this
    usage: "\u00A7cSyntax error! Do '/variablehelp' to get help on how to use this command"
    aliases: [limitlessasyncwhile]
  */

}
