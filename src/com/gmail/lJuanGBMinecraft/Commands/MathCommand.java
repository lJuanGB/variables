package com.gmail.lJuanGBMinecraft.Commands;

import org.apache.commons.lang.Validate;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.Variable;
import com.gmail.lJuanGBMinecraft.Variable.ToStringType;
import com.gmail.lJuanGBMinecraft.VariableMap;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class MathCommand extends AbstractCommand{
	
	@Override
	public boolean command( String[] args, CommandSender sender) {
		
		Validate.isTrue(args.length > 2, "At least 3 arguments must be defined");

		ImmutableLocation loc = MiscUtils.getImmutableLocation(sender);

		String varStr = ParseUtils.replaceIndex(args[0], loc.getWorld());
		Variable var = VariableMap.getVariable(loc.getWorld(), varStr);

		String[] trimedArray = MiscUtils.subarray(args, 2, args.length);
		String numberedJoined = ParseUtils.replaceVars(trimedArray, loc, ToStringType.PARENTHESES);
		
		double value = MiscUtils.operate(numberedJoined);
		
		var.set(value);
		return true;
	}
	
}