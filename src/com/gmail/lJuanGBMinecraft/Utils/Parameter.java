package com.gmail.lJuanGBMinecraft.Utils;

import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.VariableMap.VariableKey;
import com.google.common.collect.Maps;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;

public class Parameter<T> {

	private static Map<String, Parameter<?>> params = Maps.newHashMap();
	
	
	public static final Parameter<String> STRING = new Parameter<String>(false, "String", (s, l) -> true, (s, l) -> s );
	
	
	public static final Parameter<Integer> INTEGER = new Parameter<Integer>(
			false, 
			"Integer",
			(s, l) -> Ints.tryParse(s) != null,
			(s, l) -> Integer.parseInt(s));
	
	
	public static final Parameter<Double> DOUBLE = new Parameter<Double>(
			false,
			"Number",
			(s, l) -> Doubles.tryParse(s) != null,
			(s, l) -> Double.parseDouble(s));
	
	
	public static final Parameter<Player> PLAYER = new Parameter<Player>(
			true,
			"Player",
			(s, l) -> {
				Player p = null;
				try {
					p = Bukkit.getPlayer( UUID.fromString(s) );
				} catch (IllegalArgumentException ex) {}
				
				if(p == null) p = Bukkit.getPlayerExact(s);
				
				if(p == null) return false;
				return p.getLocation().getWorld().getName().equals(l.getWorld());
				},
			
			(s, l) -> {
				try {
					return Bukkit.getPlayer( UUID.fromString(s) );
				} catch (IllegalArgumentException ex2) {}
				
				return Bukkit.getPlayerExact(s);
				});
	
	
	public static final Parameter<Entity> ENTITY = new Parameter<Entity>(
			true,
			"Entity",
			(s, l) -> {
				try {
					Entity ent = Bukkit.getEntity( UUID.fromString(s) );
					
					return ent.getLocation().getWorld().getName().equals(l.getWorld());
					
				} catch (IllegalArgumentException ex3) {}
				
				return false;
				},
			
			(s, l) -> Bukkit.getEntity( UUID.fromString(s) ));
	
			
	public static final Parameter<Block> BLOCK = new Parameter<Block>(
			3,
			true,
			"Block",
			(s, l) -> {
				String[] sArray = s.split(" ");
				
				try {
					ParseUtils.parseLoc(sArray, l);
					return true;
				} catch (IllegalArgumentException ex4) {}
				
				return false;
				},
			
			(s, l) -> {
				
				double[] coords = ParseUtils.parseLoc(s.split(" "), l);
				
				World world = Bukkit.getWorld(l.getWorld());
				Location loc = new Location(world, coords[0], coords[1], coords[2], l.getYaw(), l.getPitch());
				System.out.println(loc);
				return loc.getBlock();
				});
	
	
	public static final Parameter<VariableKey> VARIABLE = new Parameter<VariableKey>(
			true,
			"Variable",
			(s, l) -> VariableKey.isKey(s),
			(s, l) -> new VariableKey(l.getWorld(), s));
	
	
	
	private final BiPredicate<String, ImmutableLocation> canParse;
	private final BiFunction<String, ImmutableLocation, T> parse;
	private final boolean usesLocation;
	private final String name;
	private final int words;
	
	
	protected Parameter(int words, boolean usesLocation, String name, BiPredicate<String, ImmutableLocation> canParse, 
			BiFunction<String, ImmutableLocation, T> parse){
		
		this.usesLocation = usesLocation;
		this.canParse = canParse;
		this.parse = parse;
		this.name = name;
		this.words = words;
		
		params.put(name.toUpperCase(), this);
	}
	
	protected Parameter(boolean usesLocation, String name, BiPredicate<String, ImmutableLocation> canParse, 
			BiFunction<String, ImmutableLocation, T> parse){
		this(1, usesLocation, name, canParse, parse);
	}
	
	
	
	public boolean canParse(String string, ImmutableLocation loc) {
		return canParse.test(string, loc);
	}
	
	public T parse(String string, ImmutableLocation loc) {
		return parse.apply(string, loc);
	}
	
	public T parse(String string) {
		Validate.isTrue(!usesLocation, "This parameter uses a location to parse it.");
		
		return parse.apply(string, null);
	}
	
	public boolean usesLocation() {
		return this.usesLocation;
	}


	public int getWords() {
		return words;
	}
	
	
	@Override
	public String toString() {
		return name;
	}

	
	public static Parameter<?> fromString(String s){
		
		Validate.isTrue(params.containsKey(s.toUpperCase()), "The parameter type " + s + " does not exist.");
		
		return params.get(s.toUpperCase());
	}
	
	
	public static int getExpectedWords(Parameter<?>...parameters) {
		
		int expectedLength = 0;
		
		for(Parameter<?> p : parameters) {
			expectedLength += p.getWords();
		}
		
		return expectedLength;
	}
}
