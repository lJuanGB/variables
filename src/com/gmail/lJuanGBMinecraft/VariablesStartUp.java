package com.gmail.lJuanGBMinecraft;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.lJuanGBMinecraft.AsyncCommands.StartAsyncCommand;
import com.gmail.lJuanGBMinecraft.Commands.ExecuteBlockCommand;
import com.gmail.lJuanGBMinecraft.Commands.ForCommand;
import com.gmail.lJuanGBMinecraft.Commands.GetDataCommand;
import com.gmail.lJuanGBMinecraft.Commands.IfCommand;
import com.gmail.lJuanGBMinecraft.Commands.MathCommand;
import com.gmail.lJuanGBMinecraft.Commands.ResetVariableCommand;
import com.gmail.lJuanGBMinecraft.Commands.StringCommand;
import com.gmail.lJuanGBMinecraft.Commands.VariableCommand;
import com.gmail.lJuanGBMinecraft.Commands.VariableHelpCommand;
import com.gmail.lJuanGBMinecraft.Commands.VariableInfoCommand;
import com.gmail.lJuanGBMinecraft.Commands.WhileCommand;
import com.gmail.lJuanGBMinecraft.Listeners.CommandBlockBreakListener;

public class VariablesStartUp extends JavaPlugin implements CommandExecutor{

	public static VariablesStartUp plugin;
	
	@Override
	public void onEnable() {
		
		plugin = this;
		
		VariableMap.start();
		
		Bukkit.getPluginManager().registerEvents(new CommandBlockBreakListener(), this);
		
		getCommand("test").setExecutor(this);
		
		getCommand("math").setExecutor(new MathCommand());
		getCommand("variable").setExecutor(new VariableCommand());
		getCommand("if").setExecutor(new IfCommand());
		getCommand("variableinfo").setExecutor(new VariableInfoCommand());
		getCommand("executeblock").setExecutor(new ExecuteBlockCommand());
		getCommand("for").setExecutor(new ForCommand());
		getCommand("while").setExecutor(new WhileCommand());
		getCommand("resetvariable").setExecutor(new ResetVariableCommand());
		getCommand("variablehelp").setExecutor(new VariableHelpCommand());

		getCommand("startasync").setExecutor(new StartAsyncCommand());
//		getCommand("amath").setExecutor(new AsyncMathCommand());
//		getCommand("aif").setExecutor(new AsyncIfCommand());
//		getCommand("aexecuteblock").setExecutor(new AsyncExecuteBlockCommand());
//		getCommand("aparticle").setExecutor(new AsyncParticleCommand());
//		getCommand("afor").setExecutor(new AsyncForCommand());
//		getCommand("awhile").setExecutor(new AsyncWhileCommand());
//		getCommand("avariable").setExecutor(new AsyncVariableCommand());

		getCommand("limitlessfor").setExecutor(new ForCommand(Integer.MAX_VALUE));
		getCommand("limitlesswhile").setExecutor(new WhileCommand(Integer.MAX_VALUE));
//		getCommand("limitlessafor").setExecutor(new AsyncForCommand(Integer.MAX_VALUE));
//		getCommand("limitlessawhile").setExecutor(new AsyncWhileCommand(Integer.MAX_VALUE));
		
		getCommand("string").setExecutor(new StringCommand());
		getCommand("getdata").setExecutor(new GetDataCommand());

	}
	
	@Override
	public void onDisable() {
		VariableMap.remove();
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
		
		Player player = (Player) sender;
		Location loc = player.getLocation();
		
		
		
		for(Entity ent : loc.getWorld().getNearbyEntities(loc, 2, 2, 2)) {
			String s = ReflectionToStringBuilder.toString(ent, RecursiveToStringStyle.JSON_STYLE);
			getConfig().set(ent.getName(), s);
			saveConfig();
		}
		
		return true;
	}
}
