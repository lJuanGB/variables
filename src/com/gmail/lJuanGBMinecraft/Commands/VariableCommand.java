package com.gmail.lJuanGBMinecraft.Commands;

import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.Utils.ExecuteUtils;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class VariableCommand extends AbstractCommand{
	
	@Override
	public boolean command(String[] args, CommandSender sender) {

		if(args.length == 0) return false;

		ImmutableLocation loc = MiscUtils.getImmutableLocation(sender);

		String commandLine = ParseUtils.replaceVars(args, loc);
		ExecuteUtils.executeCommands(sender, commandLine);
		
		return true;
	}
	
}
