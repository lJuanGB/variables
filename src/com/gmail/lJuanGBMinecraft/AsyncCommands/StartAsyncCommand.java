package com.gmail.lJuanGBMinecraft.AsyncCommands;

import com.gmail.lJuanGBMinecraft.Utils.ExecuteUtils;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;

public class StartAsyncCommand extends AbstractAsyncCommand{

	@Override
	public boolean command(String[] args, ImmutableLocation loc) {

		ExecuteUtils.executeAsyncCommands(loc, MiscUtils.join(args));
		
		return true;
	}
}
