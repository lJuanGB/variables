package com.gmail.lJuanGBMinecraft;

import com.google.common.primitives.Doubles;

/**
 * Variable object that stores the double.
 * 
 * @author lJuanGB
 */
public class Variable{

	private String value;
	
	
	public Variable(double value) {
		
		set(value);
	}
	
	public Variable(String value) {
		this.value = value;
	}
	
	public Variable(Variable var) {
		this(var.toString());
	}
	
	
	
	public void set(double value) {
		
		if(value == (int) value) {
			
			set("" + ((int) value));
			
		} else {
			
			set("" + value);
		}
	}
	
	public void set(String value) {
		
		if(value.equals("true")) {
			
			this.set(1);
			
		} else if(value.equals("false")) {
			
			this.set(0);
			
		} else {
			
			this.value = value;
		}
		
	}
	
	
	public String getValue() {
		return value;
	}
	
	public double getValueAsDouble() {

		Double result = Doubles.tryParse(value);
		
		return result == null ? Double.NaN : result;
	}
	
	
	public boolean isDouble() {
		
		return Doubles.tryParse(value) != null;
	}
	
	public boolean isString() {
		
		return !isDouble();
	}
	

	public static final String indexBegin = "[";
	public static final String indexEnd = "]";
	
	public static final String varBegin = "var.";
	public static final String varEnd = "/";
	
	
	@Override
	public String toString() {
		
		return toString(ToStringType.NORMAL);
	}
	
	public String toString(ToStringType type) {
		switch(type) {
		
		case QUOTED: return "\"" + value + "\"";
		case PARENTHESES: return "(" + value + ")";
		
		default: return value;
		}
	}
	
	
	public static enum ToStringType{
		NORMAL,
		QUOTED,
		PARENTHESES, 
	}
}
