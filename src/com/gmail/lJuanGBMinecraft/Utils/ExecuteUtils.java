package com.gmail.lJuanGBMinecraft.Utils;

import java.util.Arrays;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.AsyncCommands.AsyncCommand;

public class ExecuteUtils {

	
	public static void executeCommands(CommandSender sender, String... commands) throws IllegalArgumentException{
				
		for(String command : commands) {
			
			if(command.startsWith("/")) command = command.replaceFirst("/", "");

			try {
				Bukkit.getServer().dispatchCommand(sender, command);
			} catch(CommandException ex) {
				throw new IllegalArgumentException(ex);
			}
		}
		
	}
	
	public static void executeAsyncCommands(ImmutableLocation loc, String... commands) throws IllegalArgumentException{
				
		for(String command : commands) {
			String[] separated = command.split(" ");
			
			String cmdName = separated[0].toLowerCase().replaceFirst("/", "");

			AsyncCommand cmd = AsyncCommand.byName(cmdName);
			
			Validate.notNull(cmd, "Cannot find async command: " + cmdName);
			
			String[] args = Arrays.copyOfRange(separated, 1, separated.length);
			
			cmd.runCommand(args, loc);
		}
		
	}
}
