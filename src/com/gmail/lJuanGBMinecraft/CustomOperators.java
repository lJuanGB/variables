package com.gmail.lJuanGBMinecraft;

import java.util.ArrayList;
import java.util.List;

import net.objecthunter.exp4j.operator.Operator;

public class CustomOperators {

	public static List<Operator> customOperators;
	
	static {
		customOperators = new ArrayList<Operator>();
		
		
		customOperators.add( new Operator("==", 2, true, 400) {
			@Override
			public double apply(double... args) {
				
				if(args[0] == args[1]) return 1;
				return 0;
			}
		});
		
		
		customOperators.add( new Operator("!=", 2, true, 400) {
			@Override
			public double apply(double... args) {
				
				if(args[0] != args[1]) return 1;
				return 0;
			}
		});
		
		
		customOperators.add( new Operator("<", 2, true, 400) {
			@Override
			public double apply(double... args) {
				
				if(args[0] < args[1]) return 1;
				return 0;
			}
		});
		
		
		customOperators.add( new Operator(">", 2, true, 400) {
			@Override
			public double apply(double... args) {
				
				if(args[0] > args[1]) return 1;
				return 0;
			}
		});
		
		
		customOperators.add( new Operator("<=", 2, true, 400) {
			@Override
			public double apply(double... args) {
				
				if(args[0] <= args[1]) return 1;
				return 0;
			}
		});
		
		
		customOperators.add( new Operator(">=", 2, true, 400) {
			@Override
			public double apply(double... args) {
				
				if(args[0] >= args[1]) return 1;
				return 0;
			}
		});
		
		
		customOperators.add( new Operator("&&", 2, true, 300) {
			@Override
			public double apply(double... args) {
				
				if((args[0] != 0) && (args[1] != 0)) return 1;
				return 0;
			}
		});
		
		
		customOperators.add( new Operator("||", 2, true, 300) {
			@Override
			public double apply(double... args) {
				
				if((args[0] != 0) || (args[1] != 0)) return 1;
				return 0;
			}
		});
		
		
		customOperators.add( new Operator("!", 1, true, 450) {
			@Override
			public double apply(double... args) {
				
				if((args[0] == 0)) return 1;
				return 0;
			}
		});
	}

}
