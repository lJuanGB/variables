package com.gmail.lJuanGBMinecraft.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.CommandBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class CommandBlockBreakListener implements Listener{

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
	public void onPlayerBreakCB(BlockBreakEvent e) {
		
		if(!isCB(e.getBlock().getType())) return;
		
		CommandBlock b = (CommandBlock) e.getBlock().getState();
		
		if(b.getCommand().isEmpty()) return;
		
		Material tool = e.getPlayer().getInventory().getItemInMainHand().getType();
		
		if(tool.equals(Material.WOODEN_SHOVEL)) return;
		
		e.getPlayer().sendMessage(ChatColor.RED + "For protection, you can only break command blocks with a wooden shovel.");
		e.setCancelled(true);
	}

	private boolean isCB(Material type) {
		switch(type) {
		case COMMAND_BLOCK:
		case REPEATING_COMMAND_BLOCK:
		case CHAIN_COMMAND_BLOCK: return true;
		default: return false;
		}
	}
}
