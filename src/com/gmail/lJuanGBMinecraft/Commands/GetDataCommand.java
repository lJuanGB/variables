package com.gmail.lJuanGBMinecraft.Commands;

import org.apache.commons.lang.Validate;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.CustomMethod;
import com.gmail.lJuanGBMinecraft.Variable;
import com.gmail.lJuanGBMinecraft.VariableMap;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.Parameter;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class GetDataCommand extends AbstractCommand{

	@Override
	public boolean command(String[] args, CommandSender sender) throws IllegalArgumentException {
		
		Validate.isTrue(args.length > 4, "At least 5 arguments must be defined");

		ImmutableLocation loc = MiscUtils.getImmutableLocation(sender);

		Variable var = VariableMap.getVariable(loc.getWorld(), args[0]);

		Parameter<?> param = Parameter.fromString(args[2]);
		
		int indexMethod = 3 + param.getWords();
		
		CustomMethod meth = CustomMethod.fromString(args[indexMethod]);
		
		String[] trimedArray = MiscUtils.subarray(args, 3, args.length);
		String[] removedMethodArray = MiscUtils.remove(trimedArray, indexMethod - 3);
		String numberedJoined = ParseUtils.replaceVars(removedMethodArray, loc);
		String[] separated = ParseUtils.separateStrings(numberedJoined);						
		String[] methArgs = ParseUtils.parseParameters(separated, meth.getParameters());
		
		Validate.isTrue(meth.isValid(methArgs, loc), meth.getInValidReason(methArgs, loc));

		String result = meth.apply(methArgs, loc);
		
		var.set(result);
		return true;
	}

}
