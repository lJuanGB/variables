package com.gmail.lJuanGBMinecraft.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.VariableMap;
import com.gmail.lJuanGBMinecraft.VariableMap.VariableKey;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;

public class ResetVariableCommand extends AbstractCommand{
	
	@Override
	public boolean command(String[] args, CommandSender sender) {
		
		Location loc = MiscUtils.getLocation(sender);
		
		if(args.length == 0) {
			
			VariableMap.removeInWorld(loc.getWorld());
			sender.sendMessage(ChatColor.GOLD + "Removed all variables in the world: " + ChatColor.WHITE+ loc.getWorld().getName());

		} else if(args[0].endsWith(".*")) {
			
			VariableKey key = new VariableKey(loc.getWorld(), args[0].replaceAll(".*$", ""));
			VariableMap.removeUnderKey(key);
			sender.sendMessage(ChatColor.GOLD + "Removed the variable: " + ChatColor.WHITE+ key.toString());

		} else if(args[0].contains(".")) {
			
			VariableKey key = new VariableKey(loc.getWorld(), args[0]);
			VariableMap.removeKey(key);
			sender.sendMessage(ChatColor.GOLD + "Removed the variable: " + ChatColor.WHITE+ key.toString());
			
		} else if(args[0].equals("server")) {
			
			VariableMap.reset();
			sender.sendMessage(ChatColor.GOLD + "Removed all variables in the server");
		
		} else {
			
			VariableMap.removeInProject(loc.getWorld(), args[0]);
			sender.sendMessage(ChatColor.GOLD + "Removed all variables in the project: " + ChatColor.WHITE + args[0]);
			
		}
		
		return true;
	}
	
}
