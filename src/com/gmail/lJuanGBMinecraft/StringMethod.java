package com.gmail.lJuanGBMinecraft;

import static com.gmail.lJuanGBMinecraft.Utils.Parameter.INTEGER;
import static com.gmail.lJuanGBMinecraft.Utils.Parameter.STRING;

import java.util.function.Function;

import com.gmail.lJuanGBMinecraft.Utils.Parameter;

public enum StringMethod {

	/* ----- STRING METHODS ---- */ 
	CONCAT((s) -> s[0] + s[1], STRING, STRING), 
	CHARAT((s) -> s[0].charAt(INTEGER.parse(s[1])) + "", STRING, INTEGER), 
	CONTAINS((s) -> s[0].contains(s[1]) ? "1" : "0", STRING, STRING), 
	ENDSWITH((s) -> s[0].endsWith(s[1]) ? "1" : "0", STRING, STRING), 
	EQUALS((s) -> s[0].equals(s[1]) ? "1" : "0", STRING, STRING), 
	EQUALSIGNORECASE((s) -> s[0].equalsIgnoreCase(s[1]) ? "1" : "0", STRING, STRING), 
	INDEXOF((s) -> s[0].indexOf(s[1]) + "", STRING, STRING), 
	INDEXOFFROM((s) -> s[0].indexOf(s[1], INTEGER.parse(s[2])) + "", STRING, STRING, INTEGER),
	LASTINDEXOF((s) -> s[0].lastIndexOf(s[1]) + "", STRING, STRING),
	LASTINDEXOFFROM((s) -> s[0].lastIndexOf(s[1], INTEGER.parse(s[2])) + "", STRING, STRING, INTEGER), 
	LENGTH((s) -> s[0].length() + "", STRING), 
	MATCHES((s) -> s[0].matches(s[1]) ? "1" : "0", STRING, STRING), 
	REPLACEALL((s) -> s[0].replaceAll(s[1], s[2]), STRING, STRING, STRING), 
	REPLACEFIRST((s) -> s[0].replaceFirst(s[1], s[2]), STRING, STRING, STRING), 
	STARTSWITH((s) -> s[0].startsWith(s[1]) ? "1" : "0", STRING, STRING), 
	SUBSTRING((s) -> s[0].substring(INTEGER.parse(s[1]), INTEGER.parse(s[2])), STRING, INTEGER, INTEGER), 
	TOLOWERCASE((s) -> s[0].toLowerCase() , STRING), 
	TOUPPERCASE((s) -> s[0].toUpperCase(), STRING),
	TRIM((s) -> s[0].trim() , STRING), 

	;
	
	private final Function<String[], String> method;
	private final Parameter<?>[] params;
	
	
	private StringMethod(Function<String[], String> method, Parameter<?>... params) {
		this.method = method;
		this.params = params;

	}
	
	
	public boolean isValid(String[] parameters) {
		return getInValidReason(parameters) == null;
	}
	
	public String getInValidReason(String[] parameters) {
		
		
		if(parameters.length != Parameter.getExpectedWords(params)) {
			return "Expected " + Parameter.getExpectedWords(params) + " arguments, found " + parameters.length;
		}
		
		for(int i = 0; i < params.length; i++ ) {
			
			if(!params[i].canParse(parameters[i], null)) {
				return "Parameter " + parameters[i] + " cannot be understood as a " + params[i];
			}
		}
		
		return null;
	}
	
	public String apply(String[] parameters) {
		return method.apply(parameters);
	}
	
	
	public static StringMethod fromString(String s) {
		
		try {
			return valueOf(s.toUpperCase());
			
		} catch ( IllegalArgumentException ex) {
			
			throw new IllegalArgumentException("The method " + s + " does not exist");
		}
	}
}
