package com.gmail.lJuanGBMinecraft.AsyncCommands;

import org.apache.commons.lang.Validate;

import com.gmail.lJuanGBMinecraft.Utils.ExecuteUtils;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;

public class AsyncExecuteBlockCommand extends AbstractAsyncCommand{	
	
	@Override
	public boolean command(String[] args, ImmutableLocation loc) {
		
		int l = args.length;
		
		Validate.isTrue(l != 0, "You need at least 1 argument");
		Validate.isTrue(args[0].startsWith("{"), "The arguments must start with {");
		Validate.isTrue(args[l-1].endsWith("}"), "The arguments must start with {");
		
		String joined = MiscUtils.join(args);
		
		String[] commands = ParseUtils.separateCommands(joined);
		
		ExecuteUtils.executeAsyncCommands(loc, commands);
		
		return true;
	}

}
