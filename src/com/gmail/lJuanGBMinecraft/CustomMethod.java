package com.gmail.lJuanGBMinecraft;

import static com.gmail.lJuanGBMinecraft.Utils.Parameter.*;

import java.util.function.BiFunction;

import org.apache.commons.lang3.Validate;

import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.Parameter;

public enum CustomMethod {

	TYPE((s, l) -> type(s, l), BLOCK),
	BLOCKDATA((s, l) -> blockdata(s, l), BLOCK, STRING),
	HASBLOCKDATA((s, l) -> hasblockdata(s, l), BLOCK, STRING),

	;
	
	private final BiFunction<String[], ImmutableLocation, String> method;
	private final Parameter<?>[] params;
	
	
	private CustomMethod(BiFunction<String[], ImmutableLocation, String> method, Parameter<?>... params) {
		this.method = method;
		this.params = params;

	}
	
	
	public boolean isValid(String[] parameters, ImmutableLocation loc) {
		return getInValidReason(parameters, loc) == null;
	}
	
	public String getInValidReason(String[] parameters, ImmutableLocation loc) {
		
		if(parameters.length != params.length) {
			return "Expected " + params.length + " parameters, found " + parameters.length;
		}
		
		for(int i = 0; i < params.length; i++ ) {
			
			if(!params[i].canParse(parameters[i], loc)) {
				return "Parameter " + parameters[i] + " cannot be understood as a " + params[i];
			}
		}
		
		return null;
	}
	
	public String apply(String[] parameters, ImmutableLocation loc) {
		return method.apply(parameters, loc);
	}
	
	public Parameter<?>[] getParameters() {
		return params;
	}


	public static CustomMethod fromString(String s) {
		
		try {
			return valueOf(s.toUpperCase());
			
		} catch ( IllegalArgumentException ex) {
			
			throw new IllegalArgumentException("The method " + s + " does not exist");
		}
	}
	
	
	
	/* --- METHODS --- */
	public static String type(String[] s, ImmutableLocation l) {
		
		String data = BLOCK.parse(s[0], l).getBlockData().getAsString();
		
		if(data.contains("[")) {
			
			return data.substring(0, data.indexOf('[')).replaceFirst("minecraft:", "");
			
		} else {
			
			return data.replaceFirst("minecraft:", "");
		}
	}
	
	public static String blockdata(String[] s, ImmutableLocation l) {
		
		String data = BLOCK.parse(s[0], l).getBlockData().getAsString();
		
		int index = data.indexOf(s[1]);
		
		boolean valid = index > -1 && data.charAt(index + s[1].length()) == '=';
		Validate.isTrue(valid, "That block doesn't have the data " + s[1]);
		
		int indexEnd = -1;
		if(data.substring(index).contains(",")) {
			indexEnd = data.indexOf(',', index);
		} else {
			indexEnd = data.indexOf(']', index);
		}
		
		return data.substring(index + s[1].length() +1, indexEnd);
	}
	
	public static String hasblockdata(String[] s, ImmutableLocation l) {
		
		String data = BLOCK.parse(s[0], l).getBlockData().getAsString();
		
		return data.contains(s[1] + "=") ? "1" : "0";
	}
}
