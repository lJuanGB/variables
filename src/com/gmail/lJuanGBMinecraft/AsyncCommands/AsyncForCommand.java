package com.gmail.lJuanGBMinecraft.AsyncCommands;

import static com.gmail.lJuanGBMinecraft.Commands.ForCommand.doId;
import static com.gmail.lJuanGBMinecraft.Commands.ForCommand.fromId;
import static com.gmail.lJuanGBMinecraft.Commands.ForCommand.stepId;
import static com.gmail.lJuanGBMinecraft.Commands.ForCommand.toId;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.Validate;

import com.gmail.lJuanGBMinecraft.Variable;
import com.gmail.lJuanGBMinecraft.VariableMap;
import com.gmail.lJuanGBMinecraft.Utils.ExecuteUtils;
import com.gmail.lJuanGBMinecraft.Utils.ImmutableLocation;
import com.gmail.lJuanGBMinecraft.Utils.MiscUtils;
import com.gmail.lJuanGBMinecraft.Utils.ParseUtils;


public class AsyncForCommand extends AbstractAsyncCommand{
	
	private int limit;
	
	public AsyncForCommand(int limit) {
		this.limit = limit;
	}
	
	public AsyncForCommand() {
		this(1001);
	}
	
	
	@Override
	public boolean command(String[] args, ImmutableLocation loc) {
				
		int indexFrom = ArrayUtils.indexOf(args, fromId);
		int indexTo   = ArrayUtils.indexOf(args, toId);
		int indexStep = ArrayUtils.indexOf(args, stepId);
		int indexDo   = ArrayUtils.indexOf(args, doId);
		
		Validate.isTrue(indexFrom >= 0 && indexTo >= 0 && indexStep >= 0 && indexDo >= 0, 
				String.format("The for command should contain %s, %s, %s, %s", fromId, toId, stepId, doId));
		
		Validate.isTrue(indexFrom == 1, "The " + fromId + " should be at the second argument position.");
		
		Validate.isTrue(indexFrom < indexTo && indexTo < indexStep && indexStep < indexDo,
				String.format("The for command should contain %s, %s, %s, %s in that order", fromId, toId, stepId, doId));
		
		String varString = args[0];
		
		Variable var = VariableMap.getVariable(loc.getWorld(), varString);
		 
		
		String[] fromArgs = MiscUtils.subarray(args, indexFrom +1, indexTo);
		String[] toArgs = MiscUtils.subarray(args, indexTo +1, indexStep);
		String[] stepArgs = MiscUtils.subarray(args, indexStep +1, indexDo);

		double from;
		double to;
		double step;
		

		from = MiscUtils.operate( ParseUtils.replaceVars(fromArgs, loc) );
		
		to = MiscUtils.operate( ParseUtils.replaceVars(toArgs, loc) );

		step = MiscUtils.operate( ParseUtils.replaceVars(stepArgs, loc) );

		Validate.isTrue((to > from && step > 0) || (to < from && step < 0), "The loop would be infinite.");
		
		
		String[] command = MiscUtils.subarray(args, indexDo + 1, args.length);
		
		String joinedNumbered = MiscUtils.join( command );
		if(joinedNumbered.startsWith("{")) joinedNumbered = "/executeblock " + joinedNumbered;
		
		int count = 0;
		if(to > from) {
			
			for(double d = from ; d < to; d += step) {
				
				var.set(d);
				ExecuteUtils.executeAsyncCommands(loc, joinedNumbered);

				count++;
				Validate.isTrue(count < limit, "REACHED SECURITY LOOP LIMIT: " + limit);
			}
			
		} else {
			
			for(double d = from ; d > to; d += step) {
				
				var.set(d);
				ExecuteUtils.executeAsyncCommands(loc, joinedNumbered);

				count++;
				Validate.isTrue(count < limit, "REACHED SECURITY LOOP LIMIT: " + limit);
			}
		}

		return true;
		
	}
}
